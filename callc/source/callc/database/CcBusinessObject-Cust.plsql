-----------------------------------------------------------------------------
--
--  Logical unit: CcBusinessObject
--  Component:    CALLC
--
--  IFS Developer Studio Template Version 3.0
--
--  Date    Sign    History
--  ------  ------  ---------------------------------------------------------
-----------------------------------------------------------------------------

layer Cust;

-------------------- PUBLIC DECLARATIONS ------------------------------------

PROCEDURE henriktest 
IS 
BEGIN
   NULL;
END henriktest;

@Overtake Core
FUNCTION Get_NCR_Object_Id RETURN NUMBER
IS
   business_object_id_ NUMBER;
   business_object_id1_ NUMBER;
   CURSOR get_attr IS
       select business_object_id
       from CC_BUSINESS_OBJECT_TAB 
       WHERE cc_object_type = 'NCR';   
BEGIN
   OPEN get_attr;
   FETCH get_attr INTO business_object_id_;
   CLOSE get_attr;
   RETURN business_object_id_;
END Get_NCR_Object_Id;
-------------------- PRIVATE DECLARATIONS -----------------------------------


-------------------- LU SPECIFIC IMPLEMENTATION METHODS ---------------------


-------------------- LU SPECIFIC PRIVATE METHODS ----------------------------


-------------------- LU SPECIFIC PROTECTED METHODS --------------------------


-------------------- LU SPECIFIC PUBLIC METHODS -----------------------------


-------------------- LU CUST NEW METHODS -------------------------------------
